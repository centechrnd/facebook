<?php
require_once __DIR__ . '/config.php';



$helper = $fb->getRedirectLoginHelper();
$permissions = ['email', 'user_managed_groups']; // optional
$loginUrl = $helper->getLoginUrl('http://php.khongkhodau.com/facebook/login-callback.php', $permissions);


try {
  $accessToken = $helper->getAccessToken();
} catch(Facebook\Exceptions\FacebookResponseException $e) {
  // When Graph returns an error
  echo 'Graph returned an error: ' . $e->getMessage();
  exit;
} catch(Facebook\Exceptions\FacebookSDKException $e) {
  // When validation fails or other local issues
  echo 'Facebook SDK returned an error: ' . $e->getMessage();
  exit;
}

if (isset($accessToken)) {
	echo $accessToken;
  // Logged in!
  $_SESSION['facebook_access_token'] = (string) $accessToken;

  // Now you can redirect to another page and use the
  // access token from $_SESSION['facebook_access_token']
} else {
	echo '<a href="' . $loginUrl . '">Log in with Facebook!</a>';
}